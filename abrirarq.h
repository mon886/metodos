#ifndef ABRIRARQ_H
#define ABRIRARQ_H

#include <QDialog>
#include <vector>
#include <qdir.h>
#include <iostream>
#include <QListWidgetItem>
#include <QListWidget>
#include <qlistwidget.h>
#include <stdlib.h>
#include <qapplication.h>
#include "mensagemerro.h"

#include <dirent.h>
#include <cstdlib>
#include <iostream>
#include <string>
#include <QModelIndex>
using namespace std;

namespace Ui {
class abrirArq;
}

class abrirArq : public QDialog
{
    Q_OBJECT

public:
    explicit abrirArq(QWidget *parent = 0);
    ~abrirArq();
    void lerDiretorio(string);
    void executar(string);
    string path = "/";
    string path_saida;
    vector<string> diretorios;
    int linha;
    DIR *dir = 0;
    struct dirent *entrada = 0;
    unsigned char isDir = 0x4;
    unsigned char isFile = 0x8;


private slots:


    void on_listWidget_currentRowChanged(int currentRow);


    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_selecionar_clicked();

private:
    Ui::abrirArq *ui;
};

#endif // ABRIRARQ_H
