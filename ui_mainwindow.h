/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionBaixar_modelo;
    QAction *actionGauss;
    QAction *actionM_todo_Gauss_Jordan;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_5;
    QFrame *line_4;
    QCheckBox *botaoCalibrar;
    QHBoxLayout *horizontalLayout_2;
    QLabel *caminhaEntradaLabel;
    QLineEdit *caminhaEntrada;
    QToolButton *caminhaEntradaBotao;
    QVBoxLayout *verticalLayout_4;
    QFrame *line_2;
    QLabel *label;
    QCheckBox *gauss;
    QCheckBox *gaussJordan;
    QCheckBox *checkBox;
    QFrame *line;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_5;
    QLineEdit *nomeArquivo;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_3;
    QLineEdit *caminhaSaida;
    QToolButton *toolButton_2;
    QFrame *line_3;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *executar;
    QSpacerItem *horizontalSpacer;
    QMenuBar *menuBar;
    QMenu *menuOi;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(656, 472);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        actionBaixar_modelo = new QAction(MainWindow);
        actionBaixar_modelo->setObjectName(QString::fromUtf8("actionBaixar_modelo"));
        actionGauss = new QAction(MainWindow);
        actionGauss->setObjectName(QString::fromUtf8("actionGauss"));
        actionM_todo_Gauss_Jordan = new QAction(MainWindow);
        actionM_todo_Gauss_Jordan->setObjectName(QString::fromUtf8("actionM_todo_Gauss_Jordan"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout_2 = new QVBoxLayout(centralWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        line_4 = new QFrame(centralWidget);
        line_4->setObjectName(QString::fromUtf8("line_4"));
        line_4->setFrameShape(QFrame::HLine);
        line_4->setFrameShadow(QFrame::Sunken);

        verticalLayout_5->addWidget(line_4);

        botaoCalibrar = new QCheckBox(centralWidget);
        botaoCalibrar->setObjectName(QString::fromUtf8("botaoCalibrar"));

        verticalLayout_5->addWidget(botaoCalibrar);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        caminhaEntradaLabel = new QLabel(centralWidget);
        caminhaEntradaLabel->setObjectName(QString::fromUtf8("caminhaEntradaLabel"));

        horizontalLayout_2->addWidget(caminhaEntradaLabel);

        caminhaEntrada = new QLineEdit(centralWidget);
        caminhaEntrada->setObjectName(QString::fromUtf8("caminhaEntrada"));

        horizontalLayout_2->addWidget(caminhaEntrada);

        caminhaEntradaBotao = new QToolButton(centralWidget);
        caminhaEntradaBotao->setObjectName(QString::fromUtf8("caminhaEntradaBotao"));

        horizontalLayout_2->addWidget(caminhaEntradaBotao);


        verticalLayout_5->addLayout(horizontalLayout_2);


        verticalLayout->addLayout(verticalLayout_5);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout_4->addWidget(line_2);

        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_4->addWidget(label);

        gauss = new QCheckBox(centralWidget);
        gauss->setObjectName(QString::fromUtf8("gauss"));

        verticalLayout_4->addWidget(gauss);

        gaussJordan = new QCheckBox(centralWidget);
        gaussJordan->setObjectName(QString::fromUtf8("gaussJordan"));

        verticalLayout_4->addWidget(gaussJordan);

        checkBox = new QCheckBox(centralWidget);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));

        verticalLayout_4->addWidget(checkBox);


        verticalLayout->addLayout(verticalLayout_4);

        line = new QFrame(centralWidget);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_3->addWidget(label_2);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_8->addWidget(label_5);

        nomeArquivo = new QLineEdit(centralWidget);
        nomeArquivo->setObjectName(QString::fromUtf8("nomeArquivo"));

        horizontalLayout_8->addWidget(nomeArquivo);


        verticalLayout_3->addLayout(horizontalLayout_8);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_4->addWidget(label_3);

        caminhaSaida = new QLineEdit(centralWidget);
        caminhaSaida->setObjectName(QString::fromUtf8("caminhaSaida"));

        horizontalLayout_4->addWidget(caminhaSaida);

        toolButton_2 = new QToolButton(centralWidget);
        toolButton_2->setObjectName(QString::fromUtf8("toolButton_2"));

        horizontalLayout_4->addWidget(toolButton_2);


        verticalLayout_3->addLayout(horizontalLayout_4);


        verticalLayout->addLayout(verticalLayout_3);

        line_3 = new QFrame(centralWidget);
        line_3->setObjectName(QString::fromUtf8("line_3"));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_3);


        verticalLayout_2->addLayout(verticalLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);

        executar = new QPushButton(centralWidget);
        executar->setObjectName(QString::fromUtf8("executar"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(executar->sizePolicy().hasHeightForWidth());
        executar->setSizePolicy(sizePolicy1);
        executar->setMinimumSize(QSize(10, 10));
        executar->setMaximumSize(QSize(90, 30));

        horizontalLayout_3->addWidget(executar);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);


        verticalLayout_2->addLayout(horizontalLayout_3);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 656, 27));
        menuOi = new QMenu(menuBar);
        menuOi->setObjectName(QString::fromUtf8("menuOi"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QString::fromUtf8("toolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);
        QWidget::setTabOrder(caminhaEntradaBotao, botaoCalibrar);

        menuBar->addAction(menuOi->menuAction());
        menuOi->addAction(actionBaixar_modelo);
        menuOi->addAction(actionGauss);
        menuOi->addAction(actionM_todo_Gauss_Jordan);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Trabalho de M\303\251todos", 0, QApplication::UnicodeUTF8));
        actionBaixar_modelo->setText(QApplication::translate("MainWindow", "Mostrar Modelo", 0, QApplication::UnicodeUTF8));
        actionGauss->setText(QApplication::translate("MainWindow", "M\303\251todo Gauss ", 0, QApplication::UnicodeUTF8));
        actionM_todo_Gauss_Jordan->setText(QApplication::translate("MainWindow", "M\303\251todo Gauss-Jordan", 0, QApplication::UnicodeUTF8));
        botaoCalibrar->setText(QApplication::translate("MainWindow", "Calibrar", 0, QApplication::UnicodeUTF8));
        caminhaEntradaLabel->setText(QApplication::translate("MainWindow", "Caminho de Entrada: ", 0, QApplication::UnicodeUTF8));
        caminhaEntradaBotao->setText(QApplication::translate("MainWindow", "...", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "M\303\251todos:", 0, QApplication::UnicodeUTF8));
        gauss->setText(QApplication::translate("MainWindow", "Gauss ", 0, QApplication::UnicodeUTF8));
        gaussJordan->setText(QApplication::translate("MainWindow", "Gauss-Jordan", 0, QApplication::UnicodeUTF8));
        checkBox->setText(QApplication::translate("MainWindow", "Cramer", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "Sa\303\255da de Arquivo:", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("MainWindow", "Nome do Arquivo: ", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MainWindow", "Caminho de sa\303\255da:", 0, QApplication::UnicodeUTF8));
        toolButton_2->setText(QApplication::translate("MainWindow", "...", 0, QApplication::UnicodeUTF8));
        executar->setText(QApplication::translate("MainWindow", "Executar", 0, QApplication::UnicodeUTF8));
        menuOi->setTitle(QApplication::translate("MainWindow", "Ajuda", 0, QApplication::UnicodeUTF8));
        toolBar->setWindowTitle(QApplication::translate("MainWindow", "toolBar", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
