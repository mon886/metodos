#ifndef SALVARARQ_H
#define SALVARARQ_H

#include <QDialog>
#include <vector>
#include <qdir.h>
#include <iostream>
#include <QListWidgetItem>
#include <QListWidget>
#include <qlistwidget.h>
#include <stdlib.h>
#include <qapplication.h>
#include "mensagemerro.h"
#include <dirent.h>
#include <cstdlib>
#include <iostream>
#include <string>
#include <QModelIndex>

using namespace std;

namespace Ui {
class SalvarArq;
}

class SalvarArq : public QDialog
{
    Q_OBJECT

public:
    explicit SalvarArq(QWidget *parent = 0);
    ~SalvarArq();
    void lerDiretorio(string);
    void executar(string);
    string path = "/";
    vector<string> diretorios;
    int linha;
    DIR *dir = 0;
    struct dirent *entrada = 0;
    unsigned char isDir = 0x4;
    unsigned char isFile = 0x8;

private slots:
    void on_voltar_clicked();

    void on_abrir_clicked();

    void on_selecionar_clicked();

    void on_listWidget_currentRowChanged(int currentRow);

private:
    Ui::SalvarArq *ui;
};

#endif // SALVARARQ_H
