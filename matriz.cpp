#include "matriz.h"

Matriz::Matriz(int N){
    dimensao = N;
    matrix.resize(N);
    for (int i = 0 ; i < N ; i++ )
       matrix[i].resize(N);
}

void Matriz::carregar(vector<vector<float> > m){
    for(int i=0; i<dimensao; i++)
        for(int j=0; j<dimensao; j++)
            matrix[i][j] = m[i][j];
}

int Matriz::tamanho(){
    return dimensao;
}

void Matriz::adicionar(float v, int i , int j){
    matrix[i][j]=v;
}

void Matriz::printar(){
    for(int i=0; i<dimensao; i++){
        for(int j=0; j<dimensao; j++)
            cout << matrix[i][j] << " ";
        cout <<endl;
    }
}

vector<float> Matriz::SubstituicoesRetroativas(Matriz A, vector<float> v){
    vector<float> x;
    int n = A.tamanho();
    float soma;
    x.resize(n);
    x[n-1] = v[n-1]/A.matrix[n-1][n-1];
    for (int i=n-2; i>=0; i--){
        soma = 0;
        for (int j=i+1; j<n; j++){
            soma = soma + A.matrix[i][j]*x[j];
        }
        x[i] = (v[i] - soma) / A.matrix[i][i];
    }
    return x;
}

vector<float> Matriz::SubstituicoesSucessivas(Matriz A, vector<float> v){
    vector<float> x;
    int n = A.tamanho();
    float soma;
    x.resize(n);
    x[1] = v[1]/A.matrix[1][1];
    for (int i=1; i<n; i++){
        soma = 0;
        for (int j=0; j<i-1; j++){
            soma = soma + A.matrix[i][j]*x[j];
        }
        x[i] = (v[i] - soma) / A.matrix[i][i];
    }
    return x;
}

vector<float> Matriz::Gauss(vector<float> v){
    vector<float> d;
    float m;
    Matriz A(dimensao);
    A.carregar(matrix);

    for (int k=0 ;k< dimensao-1 ;k++){
        for (int i = k+1 ;i< dimensao ;i++){

            if ((A.matrix[k][k]==0)&&(k==dimensao-1)){
                A = pivotear(A,k,&v);
            }

            m = -A.matrix[i][k]/A.matrix[k][k];
            A.matrix[i][k] = 0;
            for (int j=k+1 ;j< dimensao ;j++)
                A.matrix[i][j] = A.matrix[i][j] + m*A.matrix[k][j];
            v[i] = v[i] + m*v[k];
        }
    }
    d = SubstituicoesRetroativas(A,v);
    return d;
}

vector<float> Matriz::GaussJordan(vector<float> v){
    vector<float> d;
    Matriz A(dimensao);
    A.carregar(matrix);

    for (int k=0 ;k< dimensao ;k++){
        for (int j = k+1 ;j< dimensao ;j++){

            if ((A.matrix[k][k]==0)&&(k==dimensao-1)){
                A = pivotear(A,k,&v);
            }

            A.matrix[k][j] = A.matrix[k][j]/A.matrix[k][k];
        }
        v[k] = v[k]/A.matrix[k][k];
        A.matrix[k][k] = 1;
        for (int i=0 ;i< dimensao ;i++){
            if(i!=k){
                for (int j=k+1 ;j< dimensao ;j++){
                    A.matrix[i][j] = A.matrix[i][j] - A.matrix[i][k]*A.matrix[k][j];
                }
                v[i] = v[i] - A.matrix[i][k]*v[k];
                A.matrix[i][k] = 0;
            }
        }
    }
    d=v;
    return d;
}

vector<float> Matriz::Cramer( vector<float> v){
    vector<float> r,aux;
    float d;
    Matriz a(dimensao);
    a.carregar(matrix);

    d = a.det();
    for (int i=0;i<dimensao;i++){
        aux=a.matrix[i]; //guadando a coluna a ser substituida;
        a.matrix[i] = v; //substituindo coluna;
        r.push_back(a.det()/d); //calculando determinante da matriz
        a.matrix[i] = aux; //restaurando matriz
    }
    return r;
}

Matriz Matriz::pivotear(Matriz A, int k,vector<float> *v)
{
    vector<float> aux_m;
    float aux_v;
    for (int i=k+1;i<dimensao;i++){
        if(A.matrix[i][i]>A.matrix[k][k]){
            aux_m = A.matrix[k];
            A.matrix[k] = A.matrix[i];
            A.matrix[i] = aux_m;

            aux_v = v->at(k);
            v->at(k) = v->at(i);
            v->at(i) = aux_v;

        }
    }

    return A;
}


float Matriz::det(){
    vector<float>v;
    float d;
    float m;
    Matriz A(dimensao);
    A.carregar(matrix);

    for (int k=0 ;k< dimensao-1 ;k++){
        for (int i = k+1 ;i< dimensao ;i++){

            if ((A.matrix[k][k]==0)&&(k==dimensao-1)){
                A = pivotear(A,k,&v);
            }

            m = -A.matrix[i][k]/A.matrix[k][k];
            A.matrix[i][k] = 0;
            for (int j=k+1 ;j< dimensao ;j++)
                A.matrix[i][j] = A.matrix[i][j] + m*A.matrix[k][j];
            //v[i] = v[i] + m*v[k];
        }
    }
    d = 1;
    //A.printar();
    for (int k=0;k<dimensao;k++){
        d = d*A.matrix[k][k];
    }
    //d = SubstituicoesRetroativas(A,v);
    //cout << d << endl;
    return d;
}


