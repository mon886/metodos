#ifndef MENSAGEMERRO_H
#define MENSAGEMERRO_H

#include <QDialog>
#include <string>


using namespace std;

namespace Ui {
class MensagemErro;
}

class MensagemErro : public QDialog
{
    Q_OBJECT

public:
    explicit MensagemErro(QWidget *parent = 0);
    ~MensagemErro();
    void executar();
    string erro;

private:
    Ui::MensagemErro *ui;
};

#endif // MENSAGEMERRO_H
