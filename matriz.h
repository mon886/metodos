#ifndef MATRIZ_H
#define MATRIZ_H

#include <vector>
#include <ctime>
#include <iostream>

using namespace std;

class Matriz{
public:
    int dimensao;
    vector<vector<float> > matrix;

    Matriz(int N);
    Matriz (int N, vector<vector<float> > m);
    float det();
    int tamanho();
    void adicionar(float v, int i , int j);
    void carregar(vector<vector<float> > m);
    void printar();
    vector<float> SubstituicoesRetroativas(Matriz A, vector<float> v);			//
    vector<float> SubstituicoesSucessivas(Matriz A, vector<float> v);			//
    vector<float> Gauss(vector<float> v);										//Gabriel (X)     //Eu peguei os pseudo-cogidos dos slides 4 e 6
    vector<float> GaussJordan(vector<float> v);									//
    vector<float> Cramer(vector<float> v);
    Matriz pivotear (Matriz A, int k, vector<float>* v);
};

#endif // MATRIZ_H
