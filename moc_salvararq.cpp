/****************************************************************************
** Meta object code from reading C++ file 'salvararq.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "salvararq.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'salvararq.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_SalvarArq[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x08,
      31,   10,   10,   10, 0x08,
      50,   10,   10,   10, 0x08,
      85,   74,   10,   10, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_SalvarArq[] = {
    "SalvarArq\0\0on_voltar_clicked()\0"
    "on_abrir_clicked()\0on_selecionar_clicked()\0"
    "currentRow\0on_listWidget_currentRowChanged(int)\0"
};

void SalvarArq::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SalvarArq *_t = static_cast<SalvarArq *>(_o);
        switch (_id) {
        case 0: _t->on_voltar_clicked(); break;
        case 1: _t->on_abrir_clicked(); break;
        case 2: _t->on_selecionar_clicked(); break;
        case 3: _t->on_listWidget_currentRowChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData SalvarArq::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SalvarArq::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_SalvarArq,
      qt_meta_data_SalvarArq, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SalvarArq::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SalvarArq::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SalvarArq::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SalvarArq))
        return static_cast<void*>(const_cast< SalvarArq*>(this));
    return QDialog::qt_metacast(_clname);
}

int SalvarArq::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
