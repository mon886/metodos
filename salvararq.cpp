#include "salvararq.h"
#include "ui_salvararq.h"

SalvarArq::SalvarArq(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SalvarArq)
{
    ui->setupUi(this);
    ui->lineEdit->setEnabled(false);
    //ui->listWidget->setCurrentRow(-1);
    executar(path);
}

SalvarArq::~SalvarArq()
{
    delete ui;
}

void SalvarArq::lerDiretorio(string nomeDir)
{

        diretorios.clear();
        ui->listWidget->clear();

        dir = opendir (nomeDir.c_str());

        if (dir == 0) {
            MensagemErro erro;
            erro.erro="Escolha uma pasta para usar o botao Abrir";
            erro.executar();
            erro.setModal(true);
            erro.exec();
        }
        else{
            while (entrada = readdir (dir)){
                diretorios.push_back(entrada->d_name);
                ui->listWidget->addItem((const QString&)entrada->d_name);

            }
        }
        closedir (dir);




}

void SalvarArq::executar(string path_local)
{

 lerDiretorio(path_local);
}



void SalvarArq::on_voltar_clicked()
{
    if(path!="/"){
        path=path.substr(0,path.size()-1);
        std::size_t found = path.find_last_of("/");
        path = path.substr(0,found+1);
        ui->lineEdit->setText(path.c_str());
        executar(path);
    }
    else{
        ui->lineEdit->setText("Impossivel voltar");
    }

}

void SalvarArq::on_abrir_clicked()
{
    if(linha>=0){
        path += diretorios.at(linha)+"/";
        ui->lineEdit->setText(path.c_str());
        executar(path);
    }
    else{
        linha +=1;
    }
}

void SalvarArq::on_selecionar_clicked()
{
    path += diretorios.at(linha)+"/";
    ui->cancelar->click();

}

void SalvarArq::on_listWidget_currentRowChanged(int currentRow)
{
    linha = currentRow;
}
