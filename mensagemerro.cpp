#include "mensagemerro.h"
#include "ui_mensagemerro.h"

MensagemErro::MensagemErro(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MensagemErro)
{
    ui->setupUi(this);
}

MensagemErro::~MensagemErro()
{
    delete ui;
}

void MensagemErro::executar()
{
    ui->erro->setText(erro.c_str());

}
