#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->caminhaEntrada->setEnabled(false);
    ui->caminhaSaida->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::Executar()
{
    verf_nome = ui->nomeArquivo->text().toUtf8().constData();
    time_t tempo, ti, tf;
    if(gaussN==false&&gaussJ==false){
        MensagemErro erro;
        erro.erro="Escolha entre Gauss ou Gauss-Jordan";
        if(verf_nome==""){
            erro.erro+="\nEscolha o nome do arquivo de saida";
        }
        erro.executar();
        erro.setModal(true);
        erro.exec();
    }
    else if(verf_nome==""){
        MensagemErro erro;
        erro.erro="Escolha o nome do arquivo de saida";
        erro.executar();
        erro.setModal(true);
        erro.exec();
    }
    else{
        //Abrir arquivo
        arquivo_S.open(path_saida.c_str(),ios::out);
        if(arquivo_S.is_open()==false){
            cout << "Não abriu" << endl;
        }
        //-------------------------------------------------------------------
        if(gaussN){

            arquivo_S << "Metodo de Gauss:" << endl;
            ti=clock();
            d=C->Gauss(v);
            tf=clock();

            arquivo_S << "Tempo de execucao :"<< (long double)(tf-ti)/CLOCKS_PER_SEC<<endl;
            arquivo_S << "Vetor coluna D:" << endl;
            for(int i=0; i<d.size(); i++){
                arquivo_S << "d" << i+1 << ": " << d[i] << endl;
            }
            arquivo_S << "Amplitudes:" << endl;
            for(int i=0; i<d.size(); i++){
                arquivo_S << "a*d" << i+1 << ": " << a*d[i] << endl;
            }
        }

        if(gaussJ){
            //--------------------------------------------------------------------
            arquivo_S << "Metodo de Gauss-Jordan:" << endl;

            ti=clock();
            d=C->GaussJordan(v);
            tf=clock();

            arquivo_S << "Tempo de execucao :"<< (long double)(tf-ti)/CLOCKS_PER_SEC<<endl;
            arquivo_S << "Vetor coluna D:" << endl;
            for(int i=0; i<d.size(); i++)
                arquivo_S << "d" << i+1  << ": " << d[i] << endl;
            arquivo_S << "Amplitudes:" << endl;
            for(int i=0; i<d.size(); i++){
                arquivo_S << "a*d" << i+1 << ": " << a*d[i] << endl;
            }

        }
        if(cramer){
             //--------------------------------------------------------------------

            arquivo_S << "Metodo de Cramer :"<<endl;
            ti=clock();
            d=C->Cramer(v);
            tf=clock();
             arquivo_S << "Tempo de execucao :"<< (long double)(tf-ti)/CLOCKS_PER_SEC<<endl;
            arquivo_S << "Vetor coluna D:" << endl;
            for (int i=0;i<d.size();i++)
                arquivo_S << "d" << i+1 << ": " << d[i]<<endl;
            arquivo_S << "Amplitudes:" << endl;
            for(int i=0; i<d.size(); i++){
                arquivo_S << "a*d" << i+1 << ": " << a*d[i] << endl;
            }

        }
        path_saida = "gedit "+path_saida;
        system(path_saida.c_str());
        arquivo_S.close();
    }

    //Fechar arquivo






}

void MainWindow::PrepararMatriz(string path)
{
        float pontoFlutuante;
        int inteiro;
        string str;
        fstream arquivo;
        v.clear();
        arquivo.open(path.c_str());
        if(arquivo.is_open()==false){
            cout << "Não abriu" << endl;
        }

        while (arquivo.is_open() && arquivo.good())
        {
            arquivo >> inteiro;
            d.resize(inteiro);
            arquivo >> a;

            C=new Matriz(d.size());
            for(int unsigned i=0;i<d.size();i++){
                for(int unsigned j=0; j<d.size();j++){
                    arquivo >> pontoFlutuante;
                     C->adicionar(pontoFlutuante,i,j);
                }
            }
            for(int unsigned i=0;i<d.size();i++){
                arquivo >> pontoFlutuante;
                v.push_back(pontoFlutuante);
            }
        }

        arquivo.close();


}

void MainWindow::on_actionBaixar_modelo_triggered()
{
    system("gedit ~/Metodos_Trab2/matriz_modelo.txt");

}

void MainWindow::on_actionGauss_triggered()
{
    system("gedit ~/Metodos_Trab2/Gauss.txt");
}

void MainWindow::on_actionM_todo_Gauss_Jordan_triggered()
{
    system("gedit ~/Metodos_Trab2/Gauss-Jordan.txt");

}



void MainWindow::on_botaoCalibrar_clicked(bool checked)
{
    if(checked){
        a = 1;
        //Vetor v:
        v.push_back(12);v.push_back(12);v.push_back(12);
        d.resize(3);

        //Matriz C:
        C=new Matriz(3);
        C->adicionar(10,0,0);C->adicionar(1,0,1);C->adicionar(1,0,2);
        C->adicionar(1,1,0);C->adicionar(10,1,1);C->adicionar(1,1,2);
        C->adicionar(1,2,0);C->adicionar(1,2,1);C->adicionar(10,2,2);

        ui->caminhaEntradaBotao->setEnabled(false);
        ui->caminhaEntradaLabel->setEnabled(false);
    }
    else{
        ui->caminhaEntradaBotao->setEnabled(true);
        ui->caminhaEntradaLabel->setEnabled(true);
    }
}

void MainWindow::on_caminhaEntradaBotao_clicked()
{
    abrirArq janelaSelecionarArquivo;
    janelaSelecionarArquivo.setModal(true);
    janelaSelecionarArquivo.exec();
    PrepararMatriz(janelaSelecionarArquivo.path.c_str());
    ui->caminhaSaida->clear();
    ui->caminhaEntrada->setText(janelaSelecionarArquivo.path.c_str());
    ui->caminhaSaida->setText(janelaSelecionarArquivo.path_saida.c_str());
}

void MainWindow::on_toolButton_2_clicked()
{
    SalvarArq janelaSalvarArquivo;
    janelaSalvarArquivo.setModal(true);
//    cout << janelaSalvarArquivo.path<<endl;
    janelaSalvarArquivo.exec();
    //path_saida=janelaSalvarArquivo.path.c_str();
    //ui->caminhaSaida->setText(janelaSalvarArquivo.path.c_str());


}

void MainWindow::on_executar_clicked()
{
    Executar();

}

void MainWindow::on_gauss_clicked(bool checked)
{
    if(checked)
        gaussN = true;
    else
        gaussN = false;
}

void MainWindow::on_gaussJordan_clicked(bool checked)
{
    if(checked)
        gaussJ = true;
    else
        gaussJ = false;

}

void MainWindow::on_nomeArquivo_editingFinished()
{
    path_saida.clear();
    path_saida = path_saida+""+ui->nomeArquivo->text().toUtf8().constData();
    path_saida=path_saida+".txt";
    ui->caminhaSaida->setText(path_saida.c_str());
}

void MainWindow::on_checkBox_clicked(bool checked)
{
    if(checked)
        cramer = true;
    else
        cramer = false;
}
