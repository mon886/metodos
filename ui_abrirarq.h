/********************************************************************************
** Form generated from reading UI file 'abrirarq.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ABRIRARQ_H
#define UI_ABRIRARQ_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_abrirArq
{
public:
    QVBoxLayout *verticalLayout;
    QLineEdit *lineEdit;
    QListWidget *listWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton;
    QPushButton *selecionar;

    void setupUi(QDialog *abrirArq)
    {
        if (abrirArq->objectName().isEmpty())
            abrirArq->setObjectName(QString::fromUtf8("abrirArq"));
        abrirArq->resize(400, 300);
        verticalLayout = new QVBoxLayout(abrirArq);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lineEdit = new QLineEdit(abrirArq);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        verticalLayout->addWidget(lineEdit);

        listWidget = new QListWidget(abrirArq);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));
        listWidget->setLayoutMode(QListView::SinglePass);

        verticalLayout->addWidget(listWidget);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pushButton_2 = new QPushButton(abrirArq);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        horizontalLayout->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(abrirArq);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        horizontalLayout->addWidget(pushButton_3);

        pushButton = new QPushButton(abrirArq);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout->addWidget(pushButton);

        selecionar = new QPushButton(abrirArq);
        selecionar->setObjectName(QString::fromUtf8("selecionar"));

        horizontalLayout->addWidget(selecionar);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(abrirArq);
        QObject::connect(pushButton_2, SIGNAL(clicked()), abrirArq, SLOT(close()));

        QMetaObject::connectSlotsByName(abrirArq);
    } // setupUi

    void retranslateUi(QDialog *abrirArq)
    {
        abrirArq->setWindowTitle(QApplication::translate("abrirArq", "Abrir arquivo", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("abrirArq", "Cancelar", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("abrirArq", "Voltar", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("abrirArq", "Abrir", 0, QApplication::UnicodeUTF8));
        selecionar->setText(QApplication::translate("abrirArq", "Selecionar", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class abrirArq: public Ui_abrirArq {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ABRIRARQ_H
