#-------------------------------------------------
#
# Project created by QtCreator 2016-01-26T11:40:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Metodos_Trab2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    matriz.cpp \
    abrirarq.cpp \
    salvararq.cpp \
    mensagemerro.cpp

HEADERS  += mainwindow.h \
    matriz.h \
    abrirarq.h \
    salvararq.h \
    mensagemerro.h

FORMS    += mainwindow.ui \
    abrirarq.ui \
    salvararq.ui \
    mensagemerro.ui
