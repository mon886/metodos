/********************************************************************************
** Form generated from reading UI file 'salvararq.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SALVARARQ_H
#define UI_SALVARARQ_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_SalvarArq
{
public:
    QVBoxLayout *verticalLayout;
    QLineEdit *lineEdit;
    QListWidget *listWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *cancelar;
    QPushButton *voltar;
    QPushButton *abrir;
    QPushButton *selecionar;

    void setupUi(QDialog *SalvarArq)
    {
        if (SalvarArq->objectName().isEmpty())
            SalvarArq->setObjectName(QString::fromUtf8("SalvarArq"));
        SalvarArq->resize(400, 300);
        verticalLayout = new QVBoxLayout(SalvarArq);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        lineEdit = new QLineEdit(SalvarArq);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        verticalLayout->addWidget(lineEdit);

        listWidget = new QListWidget(SalvarArq);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));

        verticalLayout->addWidget(listWidget);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        cancelar = new QPushButton(SalvarArq);
        cancelar->setObjectName(QString::fromUtf8("cancelar"));

        horizontalLayout->addWidget(cancelar);

        voltar = new QPushButton(SalvarArq);
        voltar->setObjectName(QString::fromUtf8("voltar"));

        horizontalLayout->addWidget(voltar);

        abrir = new QPushButton(SalvarArq);
        abrir->setObjectName(QString::fromUtf8("abrir"));

        horizontalLayout->addWidget(abrir);

        selecionar = new QPushButton(SalvarArq);
        selecionar->setObjectName(QString::fromUtf8("selecionar"));

        horizontalLayout->addWidget(selecionar);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(SalvarArq);
        QObject::connect(cancelar, SIGNAL(clicked()), SalvarArq, SLOT(close()));

        QMetaObject::connectSlotsByName(SalvarArq);
    } // setupUi

    void retranslateUi(QDialog *SalvarArq)
    {
        SalvarArq->setWindowTitle(QApplication::translate("SalvarArq", "Selecionar Pasta", 0, QApplication::UnicodeUTF8));
        cancelar->setText(QApplication::translate("SalvarArq", "Cancelar", 0, QApplication::UnicodeUTF8));
        voltar->setText(QApplication::translate("SalvarArq", "Voltar", 0, QApplication::UnicodeUTF8));
        abrir->setText(QApplication::translate("SalvarArq", "Abrir", 0, QApplication::UnicodeUTF8));
        selecionar->setText(QApplication::translate("SalvarArq", "Selecionar", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SalvarArq: public Ui_SalvarArq {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SALVARARQ_H
