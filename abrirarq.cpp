#include "abrirarq.h"
#include "ui_abrirarq.h"


abrirArq::abrirArq(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::abrirArq)
{
    ui->setupUi(this);

    ui->lineEdit->setEnabled(false);
    ui->listWidget->setCurrentRow(-1);
    executar(path);
}

abrirArq::~abrirArq()
{
    delete ui;
}

void abrirArq::lerDiretorio(string nomeDir)
{

        diretorios.clear();
        ui->listWidget->clear();

        dir = opendir (nomeDir.c_str());

        if (dir == 0) {
//            path = "gedit "+path;
//            const char* oi = path.c_str();
//            system(oi);
            MensagemErro erro;
            erro.erro="Escolha uma pasta para usar o botao Abrir";
            erro.executar();
            erro.setModal(true);
            erro.exec();
            //ui->pushButton_2->click();
        }
        else{
            while (entrada = readdir (dir)){
                diretorios.push_back(entrada->d_name);
                ui->listWidget->addItem((const QString&)entrada->d_name);

            }
        }
        closedir (dir);




}

void abrirArq::executar(string path_local)
{

 lerDiretorio (path_local);
}



void abrirArq::on_listWidget_currentRowChanged(int currentRow)
{
    linha = currentRow;
}


void abrirArq::on_pushButton_clicked()
{
    if(linha>=0){
        path += diretorios.at(linha)+"/";
        ui->lineEdit->setText(path.c_str());
        executar(path);
    }
    else{
        linha +=1;
    }
}

void abrirArq::on_pushButton_3_clicked()
{
    if(path!="/"){
        path=path.substr(0,path.size()-1);
        std::size_t found = path.find_last_of("/");
        path = path.substr(0,found+1);
        ui->lineEdit->setText(path.c_str());
        executar(path);
    }
    else{
        ui->lineEdit->setText("Impossivel voltar");
    }
}

void abrirArq::on_selecionar_clicked()
{
    if(linha>=0){
        path += diretorios.at(linha)+"/";
        dir = opendir (path.c_str());
        if (dir == 0) {
            path_saida = path;
            path += diretorios.at(linha);
            ui->pushButton_2->click();

        }
        else{
            MensagemErro erro;
            erro.erro="Escolha o um arquivo, que seja .txt";
            erro.executar();
            erro.setModal(true);
            erro.exec();
        }
        closedir(dir);
    }
    else{
        linha +=1;
    }
}
