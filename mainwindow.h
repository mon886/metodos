#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "matriz.h"
#include "abrirarq.h"
#include "salvararq.h"
#include "mensagemerro.h"
#include <QMainWindow>
#include <fstream>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    fstream arquivo_S;
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void Executar();
    void PrepararMatriz(string);
    Matriz *C;
    vector<float> v,d;
    float a;
    bool gaussN=false;
    bool gaussJ=false;
    bool cramer=false;
    string verf_nome,path_saida;

private slots:
    void on_actionBaixar_modelo_triggered();

    void on_actionGauss_triggered();

    void on_actionM_todo_Gauss_Jordan_triggered();


    void on_botaoCalibrar_clicked(bool checked);

    void on_caminhaEntradaBotao_clicked();

    void on_toolButton_2_clicked();

    void on_executar_clicked();

    void on_gauss_clicked(bool checked);

    void on_gaussJordan_clicked(bool checked);

    void on_nomeArquivo_editingFinished();

    void on_checkBox_clicked(bool checked);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
